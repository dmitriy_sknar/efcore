﻿using EFCore.DB;
using System;
using System.IO;
using EFCore.BusinessLogic;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace EFCore {
    class Program {
        static void Main(string[] args) {
            // var db = GetAppDbContext();
            var db = new SampleContextFactory().CreateDbContext(new [] { "" });

            TestConnectionEF(db);
            new Controller(db).Run();

            Console.ReadKey();
        }

        private static AppDbContext GetAppDbContext() {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");

            var config = builder.Build();
            string connectionString = config.GetConnectionString("DefaultConnection");

            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            DbContextOptions<AppDbContext> options = optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(connectionString)
                .Options;

            return new AppDbContext(options);
        }

        private static void TestConnectionEF(AppDbContext db) {
            try {
                if (db.Database.CanConnect()) {
                    Console.WriteLine(@"INFO: Connection successful");
                    return;
                }

                Console.WriteLine(@"INFO: Connection failed");
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}