﻿using EFCore.Model;
using Microsoft.EntityFrameworkCore;

namespace EFCore.DB {
    public class AppDbContext : DbContext {
        public AppDbContext() {
            // EnsureDeleteCreate();
        }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {
            // EnsureDeleteCreate();
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Fridge> Fridges { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PersonFridge> PersonFridges { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<PersonProfile> UserProfiles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            // optionsBuilder.UseSqlServer("Server=(local);Database=EFCoreAppDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            // Fluent API
            base.OnModelCreating(modelBuilder);
            // modelBuilder.Entity<Person>().ToTable("People");
            // modelBuilder.Entity<Person>().Property(p => p.Id).HasColumnName("user_id");
            // modelBuilder.Entity<Person>().HasKey(p => p.Id);
            // modelBuilder.Entity<Person>().HasKey(p => new { p.PassportSerial, p.PassportNumber }); //complex key can be set only by Fluent
            modelBuilder.Entity<Person>().HasAlternateKey(p => new { p.PassportSerial, p.PassportNumber }); //unique constraint
            // modelBuilder.Entity<Person>().Property(p => p.Name).HasColumnType("varchar(200)");

            // Data initialization

            modelBuilder.Entity<Person>().HasData(
                new Person[] {
                    new Person { Id = 1, Name = "Tom", Age = 23, PassportSerial = "AA", PassportNumber = 123456 },
                    new Person { Id = 2, Name = "Alice", Age = 26, PassportSerial = "AB", PassportNumber = 123456 },
                    new Person { Id = 3, Name = "Sam", Age = 28, PassportSerial = "AC", PassportNumber = 123456 }
                });
            
            // data links
            //1 to many
            modelBuilder.Entity<Person>()
                .HasOne(p => p.Company)
                .WithMany(t => t.Persons)
                .HasForeignKey(p => p.CompanyId);

            //1 to 1
            modelBuilder
                .Entity<Person>()
                .HasOne(u => u.PersonProfile)
                .WithOne(p => p.Person)
                .HasForeignKey<PersonProfile>(p => p.PersonId);

            //many to many
            modelBuilder.Entity<PersonFridge>()
                .HasKey(pf => new { pf.FridgeId, pf.PersonId });
            modelBuilder.Entity<PersonFridge>()
                .HasOne(pf => pf.Fridge)
                .WithMany(b => b.PersonFridges)
                .HasForeignKey(pf => pf.FridgeId);
            modelBuilder.Entity<PersonFridge>()
                .HasOne(pf => pf.Person)
                .WithMany(c => c.PersonFridges)
                .HasForeignKey(pf => pf.PersonId);
        }

        private void EnsureDeleteCreate() {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }
    }
}