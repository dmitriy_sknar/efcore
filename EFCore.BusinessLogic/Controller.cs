﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCore.DB;
using EFCore.Model;

namespace EFCore.BusinessLogic {
    public class Controller {
        private AppDbContext db;

        public Controller() { }

        public Controller(AppDbContext db) {
            this.db = db;
        }

        public void Run() {
            CreateAsync().Wait();
            CRUD();
            Linq();
        }

        public void CRUD() {
            var microsoftCompany = new Company(){Name = "Microsoft"};
            Person person1 = new Person { Name = "Tom", Age = 33, PassportSerial = "AF", PassportNumber = 123456, Company = microsoftCompany };
            Person person2 = new Person { Name = "Alice", Age = 26, PassportSerial = "AJ", PassportNumber = 123456, Company = microsoftCompany };

            // Add
            db.Persons.Add(person1);
            db.Persons.Add(person2);
            db.SaveChanges();

            // Get
            List<Person> persons = db.Persons.ToList();
            Console.WriteLine("DB data:");
            foreach (var u in persons) {
                Console.WriteLine($"{u.Id}.{u.Name} - {u.Age}");
            }

            Person person = db.Persons.FirstOrDefault();
            if (person != null) {
                person.Name = "Bob";
                person.Age = 44;
                // Edit
                db.Persons.Update(person);
                db.SaveChanges();
            }

            // Get
            Console.WriteLine("\nData after edit:");
            persons = db.Persons.ToList();
            foreach (var u in persons) {
                Console.WriteLine($"{u.Id}.{u.Name} - {u.Age}");
            }

            person = db.Persons.FirstOrDefault();
            if (person != null) {
                //Delete
                db.Persons.Remove(person);
                db.SaveChanges();
            }

            // Get
            Console.WriteLine("\nDataset after delete one record:");
            persons = db.Persons.ToList();
            foreach (var u in persons) {
                Console.WriteLine($"{u.Id}. {u.Name} - {u.Age}");
            }
        }

        public async Task CreateAsync() {
            Person person1 = new Person { Name = "TomAsync", Age = 33, PassportSerial = "AD", PassportNumber = 123456 };
            Person person2 = new Person { Name = "AliceAsync", Age = 26, PassportSerial = "AE", PassportNumber = 123456 };

            // Add
            await db.Persons.AddRangeAsync(person1, person2);
            await db.SaveChangesAsync();
        }

        public void Linq() {
            //Linq for Entities
            //No real query execute. Query is created only
            var query = db.Persons.Where(p => p.Age > 27 && p.Company.Name.Equals("Microsoft"));
            //Real query execution
            var persons = query.ToList();

            Console.WriteLine("\nLinq select");
            foreach (var u in persons) {
                Console.WriteLine($"{u.Id}. {u.Name} - {u.Age} - {u.Company.Name}");
            }
        }
    }
}