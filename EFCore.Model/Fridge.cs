﻿using System.Collections.Generic;

namespace EFCore.Model {
    public class Fridge {
        public int Id { get; set; }
        public int RoomNumber { get; set; }

        public virtual ICollection<PersonFridge> PersonFridges { get; set; }
    }
}