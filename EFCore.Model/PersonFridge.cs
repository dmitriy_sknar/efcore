﻿namespace EFCore.Model {
    public class PersonFridge {
        public virtual int FridgeId { get; set; }
        public virtual Fridge Fridge { get; set; }
        public virtual int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}