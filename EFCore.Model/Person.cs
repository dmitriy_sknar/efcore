﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EFCore.Model {
    // [Table("People")]
    public class Person {

        // [Column("user_id")]
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public int Age { get; set; } = 0;

        [NotMapped]
        public string Description { get; set; }

        [Column(TypeName = "varchar(200)")]
        public string Comment { get; set; }
        
        [MaxLength(2)]
        public string PassportSerial { get; set; }
        [MaxLength(6)]
        public int PassportNumber { get; set; }

        public virtual int? CompanyId { get; set; } // external key 
        public virtual Company Company { get; set; } // navigation property


        public virtual int? PersonProfileId { get; set; } 
        public virtual PersonProfile PersonProfile { get; set; }

        public virtual ICollection<PersonFridge> PersonFridges { get; set; }

        public string Email { get; set; }

    }
}