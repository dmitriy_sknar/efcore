﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCore.Model
{
    public class PersonProfile {
        public int Id { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }

        public virtual int PersonId { get; set; }
        public virtual Person Person { get; set; }
    }
}
