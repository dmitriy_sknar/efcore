﻿using System.Collections.Generic;

namespace EFCore.Model {
    public class Company {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }
}