﻿namespace EFCore.Model {
    public class Product {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public int FridgeId { get; set; }
        public int Price { get; set; }
    }
}